package eg.edu.alexu.csd.oop.jdb;

import java.io.File;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

import eg.edu.alexu.csd.oop.jdbc.pool.ClientClass;

public class DriverImp implements Driver {

	@Override
	public boolean acceptsURL(String arg0) throws SQLException {
		return true;
	}

	@Override
	public DriverPropertyInfo[] getPropertyInfo(String arg0, Properties arg1)
			throws SQLException {
		DriverPropertyInfo[] usedMap = new DriverPropertyInfo[1];
		usedMap[0] = new DriverPropertyInfo("path", " ");
		return usedMap;
	}

	@Override
	public Connection connect(String url, Properties info) throws SQLException {
		File dir = (File) info.get("path");
		String path = dir.getAbsolutePath();
		ClientClass c = new ClientClass();
		return c.getConnection(path); // pool design pattern
		// Connection connect = new ConnectionImp(path);
		// return connect;
	}

	@Override
	public int getMajorVersion() {

		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public int getMinorVersion() {

		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {

		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean jdbcCompliant() {

		throw new java.lang.UnsupportedOperationException();
	}

}
