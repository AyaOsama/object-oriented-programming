package eg.edu.alexu.csd.oop.jdbc.pool;

import java.sql.Connection;

import eg.edu.alexu.csd.oop.jdb.ConnectionImp;
import eg.edu.alexu.csd.oop.jdb.ConnectionImp.ConnectionPool;

public class ClientClass {

	// class for requires a connection

	private ConnectionImp.ConnectionPool connectionPool;

	// Constructor
	public ClientClass() {
		connectionPool = ConnectionPool.getInstance();
	}

	// call when want to get a connection
	public Connection getConnection(String path) {

		return connectionPool.AcquireImpl(path);
	}

	// call when close connection
	public void releaseConnection(String path, Connection c) {
		connectionPool.releaseImpl(path, c);
	}
}
