package eg.edu.alexu.csd.oop.calculator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class History {

	static String[] myHistory;
	static int index;

	public History() {
		myHistory = new String[5];
		index = -1;
	}

	public void store(String s) {
		if (index == 4) {
			for (int i = 0; i < 4; i++) {
				myHistory[i] = myHistory[i + 1];
			}
			myHistory[index] = s;
		} else {
			index++;
			myHistory[index] = s;
		}

		// do operation delete all it's next
		if (index != 4 && myHistory[index + 1] != null) {
			for (int i = index + 1; i < 5; i++) {
				myHistory[i] = null;
			}
		}
	}

	public String next() {
		if (index == 4 || myHistory[index + 1] == null) {
			return null;
		}else{
		index++;
		return myHistory[index];
		}
	}

	public String prev() {
		if(index == 0 || index == -1){
			return null;
		}
		index--;
		return myHistory[index];
		
	}

	public String current() {
		if(index == -1){
			return null;
		}
		return myHistory[index];
	}

	public void save() {

		BufferedWriter write = null;
		try {
			write = new BufferedWriter(new FileWriter("file.txt"));
			if (myHistory[0] != null) {
				for (int i = 0; i <= index; i++) {
					write.write(myHistory[i]);
					write.newLine();
				}
			}
			write.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void load() {

		BufferedReader read = null;
		try {
			read = new BufferedReader(new FileReader("file.txt"));
			int num = 0;
			String line = null;
			while ((line = read.readLine()) != null) {
				myHistory[num] = line;
				index = num;
				num++;
			}
			read.close();
			for (int i = num; i < 5; i++) {
				myHistory[i] = null;
			}

		} catch (Exception e) {
			// TODO: handle exception
			throw new RuntimeException();

		}

	}
}
