package eg.edu.alexu.csd.oop.calculator;



import java.io.File;
import java.io.FileWriter;

import org.w3c.dom.ranges.RangeException;


public class Implementation implements Calculator {

	History myHistory;
	
	
	Operation add = new Addition();
	Operation subtract = new Subtraction();
	Operation multiply = new Multiplication();
	Operation divide = new Divition();

	
	public Implementation(){
	
		myHistory = new History();
		File f = new File("file.txt");
		f.delete();
		
	}
	@Override
	public void input(String s) {
		// TODO Auto-generated method stub
		
	 myHistory.store(s);
	
	}

	@Override
	public String getResult() {
		// TODO Auto-generated method stub
		
		String a[] = myHistory.current().split("(?<=[-+*/])|(?=[-+*/])");
		double x = Double.parseDouble(a[0]);
		double  y = Double.parseDouble(a[2]);
		double o = a[1].charAt(0);
		
		String result = "";
		if(o == '+'){
			result += (add.getResult(x, y)) ;
		}else if(o == '-'){
			result += (subtract.getResult(x, y)) ;
		}else if(o == '*'){
			result += (multiply.getResult(x, y));
		}else if(o == '/'){
			if(y == 0){
				throw new RuntimeException();
			}
			result += (divide.getResult(x, y));
		}
		
		
		return result;
	
	}

	@Override
	public String current() {
		// TODO Auto-generated method stub
		
		return myHistory.current();
	}

	@Override
	public String prev() {
		// TODO Auto-generated method stub
		
		return myHistory.prev();
	}

	@Override
	public String next() {
		// TODO Auto-generated method stub
		return myHistory.next();
	}

	@Override
	public void save() {
		// TODO Auto-generated method stub
		myHistory.save();
		
	}

	@Override
	public void load() {
		// TODO Auto-generated method stub
		try{
			myHistory.load();
		}catch(RangeException e){
			throw new RuntimeException();
		}
	}

}
