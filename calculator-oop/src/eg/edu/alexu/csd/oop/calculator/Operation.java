package eg.edu.alexu.csd.oop.calculator;

public abstract class Operation {

	public abstract double getResult(double x , double y);
	
}
