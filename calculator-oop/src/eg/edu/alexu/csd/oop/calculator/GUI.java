package eg.edu.alexu.csd.oop.calculator;

import javax.swing.*;

import java.awt.event.*;

public class GUI extends JFrame implements ActionListener {

	JLabel label, errorLabel;
	JTextField tf;
	JButton b1, b2, b3, b4, b5, b6, b7, b8, b9, b0, beq, bdot, badd, bsub,
			bmult, bdiv, bnext, bprev, bcurr, bsave, bload;

	Calculator myObj = new Implementation();
	boolean clear = false;

	private static GUI myGUI;

	public static GUI getMyGUI() {
		if (myGUI == null) {
			myGUI = new GUI();
		}
		return myGUI;
	}

	private GUI() {

		tf = new JTextField();
		label = new JLabel("Answer");
		errorLabel = new JLabel("");
		b1 = new JButton("1");
		b2 = new JButton("2");
		b3 = new JButton("3");
		b4 = new JButton("4");
		b5 = new JButton("5");
		b6 = new JButton("6");
		b7 = new JButton("7");
		b8 = new JButton("8");
		b9 = new JButton("9");
		b0 = new JButton("0");
		bdiv = new JButton("/");
		bmult = new JButton("*");
		bsub = new JButton("-");
		badd = new JButton("+");
		bdot = new JButton(".");
		beq = new JButton("=");
		bprev = new JButton("previous");
		bnext = new JButton("Next");
		bcurr = new JButton("Current");
		bsave = new JButton("Save");
		bload = new JButton("Load");

		tf.setBounds(30, 40, 200, 30);
		label.setBounds(250, 40, 100, 30);
		b7.setBounds(40, 100, 50, 40);
		b8.setBounds(110, 100, 50, 40);
		b9.setBounds(180, 100, 50, 40);
		bdiv.setBounds(250, 100, 50, 40);
		bsave.setBounds(310, 100, 100, 40);

		b4.setBounds(40, 170, 50, 40);
		b5.setBounds(110, 170, 50, 40);
		b6.setBounds(180, 170, 50, 40);
		bmult.setBounds(250, 170, 50, 40);
		bload.setBounds(310, 170, 100, 40);

		b1.setBounds(40, 240, 50, 40);
		b2.setBounds(110, 240, 50, 40);
		b3.setBounds(180, 240, 50, 40);
		bsub.setBounds(250, 240, 50, 40);

		bdot.setBounds(40, 310, 50, 40);
		b0.setBounds(110, 310, 50, 40);
		beq.setBounds(180, 310, 50, 40);
		badd.setBounds(250, 310, 50, 40);

		bprev.setBounds(60, 380, 100, 40);
		bnext.setBounds(180, 380, 100, 40);
		bcurr.setBounds(120, 430, 100, 40);
		errorLabel.setBounds(30, 460, 400, 100);

		this.add(tf);
		this.add(label);
		this.add(errorLabel);
		this.add(b7);
		this.add(b8);
		this.add(b9);
		this.add(bdiv);
		this.add(b4);
		this.add(b5);
		this.add(b6);
		this.add(bmult);
		this.add(b1);
		this.add(b2);
		this.add(b3);
		this.add(bsub);
		this.add(bdot);
		this.add(b0);
		this.add(beq);
		this.add(badd);
		this.add(bprev);
		this.add(bnext);
		this.add(bcurr);
		this.add(bsave);
		this.add(bload);

		this.setLayout(null);
		this.setVisible(true);
		this.setSize(450, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);

		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);
		b6.addActionListener(this);
		b7.addActionListener(this);
		b8.addActionListener(this);
		b9.addActionListener(this);
		b0.addActionListener(this);
		badd.addActionListener(this);
		bdiv.addActionListener(this);
		bmult.addActionListener(this);
		bsub.addActionListener(this);
		bdot.addActionListener(this);
		beq.addActionListener(this);
		bprev.addActionListener(this);
		bnext.addActionListener(this);
		bcurr.addActionListener(this);
		bsave.addActionListener(this);
		bload.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		if (e.getSource() == b1) {
			if (!clear) {
				tf.setText(tf.getText().concat("1"));
			} else {
				tf.setText("1");
				clear = false;
			}
		} else if (e.getSource() == b2) {
			if (!clear) {
				tf.setText(tf.getText().concat("2"));
			} else {
				tf.setText("2");
				clear = false;
			}

		} else if (e.getSource() == b3) {

			if (!clear) {
				tf.setText(tf.getText().concat("3"));
			} else {
				tf.setText("3");
				clear = false;
			}

		} else if (e.getSource() == b4) {
			if (!clear) {
				tf.setText(tf.getText().concat("4"));
			} else {
				tf.setText("4");
				clear = false;
			}

		} else if (e.getSource() == b5) {
			if (!clear) {
				tf.setText(tf.getText().concat("5"));
			} else {
				tf.setText("5");
				clear = false;
			}

		} else if (e.getSource() == b6) {
			if (!clear) {
				tf.setText(tf.getText().concat("6"));
			} else {
				tf.setText("6");
				clear = false;
			}

		} else if (e.getSource() == b7) {
			if (!clear) {
				tf.setText(tf.getText().concat("7"));
			} else {
				tf.setText("7");
				clear = false;
			}

		} else if (e.getSource() == b8) {
			if (!clear) {
				tf.setText(tf.getText().concat("8"));
			} else {
				tf.setText("8");
				clear = false;
			}

		} else if (e.getSource() == b9) {
			if (!clear) {
				tf.setText(tf.getText().concat("9"));
			} else {
				tf.setText("9");
				clear = false;
			}

		} else if (e.getSource() == b0) {
			if (!clear) {
				tf.setText(tf.getText().concat("0"));
			} else {
				tf.setText("0");
				clear = false;
			}

		} else if (e.getSource() == bdot) {
			if (!clear) {
				tf.setText(tf.getText().concat("."));
			} else {
				tf.setText(".");
				clear = false;
			}

		} else if (e.getSource() == badd) {
			tf.setText(tf.getText().concat(" + "));

		} else if (e.getSource() == bsub) {
			tf.setText(tf.getText().concat(" - "));

		} else if (e.getSource() == bmult) {
			tf.setText(tf.getText().concat(" * "));

		} else if (e.getSource() == bdiv) {
			tf.setText(tf.getText().concat(" / "));

		} else if (e.getSource() == beq) {
			clear = true;
			try {
				myObj.input(tf.getText());
				label.setText(myObj.getResult());
				errorLabel.setText("");
			} catch (RuntimeException e2) {
				errorLabel.setText("Error ; Divition by ZERO !!");
				label.setText("Answer");
			}

		} else if (e.getSource() == bnext) {

			String s = myObj.next();
			if (s == null) {
				errorLabel.setText("Error , Not Allowed");
				tf.setText("");
				label.setText("");
			} else {
				tf.setText(s);
				label.setText("");
				errorLabel.setText("");
			}

		} else if (e.getSource() == bprev) {
			String s = myObj.prev();
			if (s == null) {
				errorLabel.setText("Error , Not Allowed");
				tf.setText("");
				label.setText("");
			} else {
				tf.setText(s);
				label.setText("");
				errorLabel.setText("");
			}

		} else if (e.getSource() == bcurr) {

			tf.setText(myObj.current());
			label.setText("");
			errorLabel.setText("");

		} else if (e.getSource() == bsave) {

			myObj.save();
		} else if (e.getSource() == bload) {
			try {
				myObj.load();

			} catch (RuntimeException eLoad) {
				errorLabel.setText("Error , No any thing to load ; please save first");
				tf.setText("");
				label.setText("");
			}
		}

	}

}
